export default {
  findObject(object, callback){
    for (let key in object){
      if(callback(object[key])){
        return object[key]
      }
      return undefined
    }
  }
}
