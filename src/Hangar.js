export default class Hangar {
  constructor(){
    this._hangar = {}
    this._id = 1
    this._ids = {}
    this.idProperty = Symbol('idProperty')
  }

  add(value, type){
    if(type) return this._add(value, type)

    const name = value.__proto__.constructor.name
    if(name != 'Object') return this._add(value, name)

    throw new Error('A type is required to add an object.')
  }

  delete(instance){
    const id = instance[this.idProperty]
    const object = this._ids[id]
    delete this._hangar[object][id]
    delete this._ids[id]
    return instance
  }

  read(type, id){
    type = 'function' == typeof type ? type.name : type

    if(id) return this._hangar[type].id

    return Object.values(this._hangar[type])
  }

  readId(id){
    return this._hangar[this._ids[id]].id
  }

  _add(value, type){
    if(!this._hangar[type]){
      this._hangar[type] = []
    }
    value[this.idProperty] = this._id
    this._hangar[type][this._id] = new Proxy(value, {set:()=>{
      throw new Error('Please do not change from Hangar.')
    }})
    this._ids[this._id] = type
    this._id++
    return value
  }
}
