/* eslint no-console:"off"*/
import Launcher from './Launcher'
import LaunchLog from './LaunchLog'
import Hangar from './Hangar'
import {LAUNCHER_EVENT} from './property'

let launcher = new Launcher()

let hangar = new Hangar()
let launchLog = hangar.add(new LaunchLog())
const context = hangar.add({a:1}, 'Context')
const context2 = hangar.add({a:2}, 'Context')

function test(){console.dir(this)}
function test2(){console.log('test2')}
const launchMap = {
  [LAUNCHER_EVENT.fire]: [[launchLog.recode, launchLog]],
  test: [
    [test, context],
    test,
    [test2, context],
    test2
  ],
}
const event = launcher.setMap(launchMap)

console.dir(hangar._hangar.Context[context[hangar.idProperty]].a)
hangar.delete(context2)
console.dir(hangar)
launcher.remove(event.test, test)
launcher.fire(event.test)
launchLog.show()
let sampleFromHangar = hangar.read('Context')
console.dir(sampleFromHangar)
sampleFromHangar[0].a = 999
console.log('changed')

hangar._hangar.Context[context[hangar.idProperty]].a = 999
console.dir(hangar._hangar.Context[context[hangar.idProperty]].a)
