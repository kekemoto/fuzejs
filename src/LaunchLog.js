import {LAUNCHER_EVENT} from './property'
import Helper from './Helper'

export default class LaunchLog {
  constructor(){
    this.log = []
  }

  recode(event, args, listeners){
    if(Helper.findObject(LAUNCHER_EVENT, value => event == value)) return 0

    this.log.push({event, args, listeners})
  }

  /* eslint no-console: "off" */
  show(){
    console.dir(this.log)
  }
}
