import {LAUNCHER_EVENT} from './property'

export default class Launcher {
  constructor(){
    this._listeners = {}
    for(let event in LAUNCHER_EVENT){
      this._listeners[LAUNCHER_EVENT[event]] = []
    }
  }

  set(event, callback, instance){
    if(!this._listeners[event]){
      this._listeners[event] = []
    }
    this._listeners[event].push({callback, instance})
  }

  setMap(map){
    for (let event in map){
      map[event].forEach(listener => {
        if('function' == typeof listener){
          this.set.apply(this, [event, listener])
        } else {
          this.set.apply(this, [event].concat(listener))
        }
      })
    }

    return Launcher.map2event(map)
  }

  remove(event, callback, instance){
    if(callback){
      this._listeners[event] = this._listeners[event].filter(_listener => {
        return _listener.callback !== callback || JSON.stringify(_listener.instance) !== JSON.stringify(instance)
      })
    } else {
      this._listeners[event] = []
    }
  }

  fire(event, ...args){
    if(event != LAUNCHER_EVENT.fire){
      this.fire(LAUNCHER_EVENT.fire, event, args, this._listeners[event])
    }

    this._listeners[event].forEach(listener => {
      listener.callback.apply(listener.instance, args)
    })
  }

  static map2event(map){
    let events = {}
    for(let event in map){
      events[event] = event
    }
    return events
  }
}
