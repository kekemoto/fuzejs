const Path = require('path')

module.exports = {
  context: __dirname,

  entry: './src/index.js',

  output: {
    path: Path.resolve(__dirname, 'dist'),
    filename: 'index.js',
  },

  module: {
    rules: [{
      test: /\.js$/,
      include: "src",
      loader: 'babel-loader',
      options: {
        presets: ['es2015'],
      },
    }],
  },

  // devtool: 'cheap-eval-source-map',
  devtool: 'source-map',

  devServer: {
    contentBase: Path.resolve(__dirname, 'devServer')
  },
}
